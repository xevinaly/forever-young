package space.uncountablyinfinite.foreveryoung.common.data;

import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.RegistryKey;
import net.minecraft.world.DimensionType;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.storage.WorldSavedData;
import space.uncountablyinfinite.foreveryoung.ForeverYoung;

import java.util.*;

public class SterileAnimals {
    private final Map<RegistryKey<World> , Data> animals = new HashMap<>();

    public boolean isSterile(AnimalEntity animal) {
        RegistryKey<World> dimension = animal.level.dimension();
        if (!animals.containsKey(dimension))
            if (animal.level instanceof ServerWorld)
                initialize((ServerWorld) animal.level);
            else
                return false;
        return animals.get(dimension).isSterile(animal);
    }

    public void sterilize(AnimalEntity animal) {
        RegistryKey<World> dimension = animal.level.dimension();
        if (!animals.containsKey(dimension))
            if (animal.level instanceof ServerWorld)
                initialize((ServerWorld) animal.level);
            else
                return;
        animals.get(dimension).sterilize(animal);
    }

    public void kill(AnimalEntity animal) {
        RegistryKey<World> dimension = animal.level.dimension();
        if (!animals.containsKey(dimension))
            if (animal.level instanceof ServerWorld)
                initialize((ServerWorld) animal.level);
            else
                return;
        animals.get(dimension).kill(animal);
    }

    public void travel(AnimalEntity animal, RegistryKey<World> newDimension) {
        RegistryKey<World> oldDimension = animal.level.dimension();

        if (!animals.containsKey(oldDimension))
            if (animal.level instanceof ServerWorld)
                initialize((ServerWorld) animal.level);
            else
                return;

        if (!animals.containsKey(newDimension))
            if (animal.level instanceof ServerWorld)
                initialize((ServerWorld) animal.level);
            else
                return;


        animals.get(oldDimension).kill(animal);

        animals.get(newDimension).sterilize(animal);
    }

    private void initialize(ServerWorld world) {
        Data save = world.getDataStorage().computeIfAbsent(Data::new, Data.DATA_NAME);
        save.setDirty();

        animals.put(world.dimension(), save);
        ForeverYoung.LOGGER.info(animals);
    }

    private class Data extends WorldSavedData {
        public static final String DATA_NAME = ForeverYoung.MOD_ID + "_Sterile_Animals";

        private final List<UUID> animals = new ArrayList<>();

        public Data(){
            super(DATA_NAME);
        }

        public boolean isSterile(AnimalEntity animal) {
            return animals.contains(animal.getUUID());
        }

        public void sterilize(AnimalEntity animal) {
            animals.add(animal.getUUID());
        }

        public void kill(AnimalEntity animal) {
            animals.remove(animal.getUUID());
        }

        @Override
        public void load(CompoundNBT data) {
            data.getAllKeys().parallelStream().forEach(key -> {
                animals.add(UUID.fromString(key));
            });
        }

        @Override
        public CompoundNBT save(CompoundNBT data) {
            animals.forEach(animal -> {
                data.put(animal.toString(), new CompoundNBT());
            });
            return data;
        }
    }
}