package space.uncountablyinfinite.foreveryoung.common.items;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.particles.IParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.server.ServerWorld;
import space.uncountablyinfinite.foreveryoung.ForeverYoung;
import space.uncountablyinfinite.foreveryoung.core.init.Foods;

import java.util.Random;

public class SunflowerStew extends SoupItem {
    private final Random random = new Random();

    public SunflowerStew() {
        super(new Item.Properties().stacksTo(1).tab(ItemGroup.TAB_FOOD).food(Foods.SUNFLOWER_STEW));
    }

    public ActionResultType interactLivingEntity(ItemStack stack, PlayerEntity player, LivingEntity entity, Hand hand) {
        if (entity instanceof AnimalEntity) {
            AnimalEntity animal = (AnimalEntity) entity;

            if (ForeverYoung.STERILE_ANIMALS.isSterile(animal)) return ActionResultType.FAIL;

            if (animal.level instanceof ServerWorld) {
                if (animal.isBaby()) animal.setAge(0);
                else animal.setInLoveTime(1);

                player.inventory.setItem(player.inventory.selected, feed(stack, player));

                animal.level.playSound(null, animal.getX(), animal.getY(), animal.getZ(), animal.getEatingSound(stack), SoundCategory.NEUTRAL, 1.0F, 1.0F + (animal.level.random.nextFloat() - animal.level.random.nextFloat()) * 0.4F);
            } else {
                IParticleData particle = animal.isBaby() ? ParticleTypes.HAPPY_VILLAGER : ParticleTypes.ENTITY_EFFECT;
                double d0 = animal.isBaby() ? random.nextGaussian() * 0.2D : 0.9725490196078431D;
                double d1 = animal.isBaby() ? random.nextGaussian() * 0.2D : 0.49019607843137253D;
                double d2 = animal.isBaby() ? random.nextGaussian() * 0.2D : 0.13725490196078433D;

                for (int i = 0; i < 10; i++)
                    animal.level.addParticle(particle, animal.getRandomX(1.0D), animal.getRandomY() + 0.5D, animal.getRandomZ(1.0D), d0, d1, d2);
            }
        }

        return super.interactLivingEntity(stack, player, entity, hand);
    }

    public ItemStack feed(ItemStack stack, PlayerEntity player) {
        return player.abilities.instabuild ? stack : new ItemStack(Items.BOWL);
    }
}
