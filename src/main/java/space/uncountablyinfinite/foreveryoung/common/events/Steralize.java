package space.uncountablyinfinite.foreveryoung.common.events;

import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Items;
import net.minecraft.particles.IParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.event.entity.living.BabyEntitySpawnEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import space.uncountablyinfinite.foreveryoung.ForeverYoung;

import java.util.Random;

@Mod.EventBusSubscriber(modid = ForeverYoung.MOD_ID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class Steralize {
    private static final Random random = new Random();

    @SubscribeEvent
    public static void enforceSterility(BabyEntitySpawnEvent event) {
        if ((event.getParentA() instanceof AnimalEntity && ForeverYoung.STERILE_ANIMALS.isSterile((AnimalEntity) event.getParentA())) || (event.getParentB() instanceof AnimalEntity && ForeverYoung.STERILE_ANIMALS.isSterile((AnimalEntity) event.getParentB())))
            event.setCanceled(true);
    }

    @SubscribeEvent
    public static void feedPoisonousPotato(PlayerInteractEvent.EntityInteract event) {
        if (event.getItemStack().getItem() == Items.POISONOUS_POTATO && event.getTarget() instanceof AnimalEntity) {
            AnimalEntity animal = (AnimalEntity) event.getTarget();
            PlayerEntity player = (PlayerEntity) event.getEntityLiving();

            if (event.getWorld() instanceof ServerWorld) {
                if (!player.abilities.instabuild) event.getItemStack().shrink(1);
                ForeverYoung.STERILE_ANIMALS.sterilize(animal);
                animal.level.playSound(null, animal.getX(), animal.getY(), animal.getZ(), animal.getEatingSound(event.getItemStack()), SoundCategory.NEUTRAL, 1.0F, 1.0F + (animal.level.random.nextFloat() - animal.level.random.nextFloat()) * 0.4F);
            } else {
                IParticleData particle = animal.isBaby() ? ParticleTypes.ANGRY_VILLAGER : ParticleTypes.ENTITY_EFFECT;
                double d0 = animal.isBaby() ? random.nextGaussian() * 0.2D : 0.3058823529411765D;
                double d1 = animal.isBaby() ? random.nextGaussian() * 0.2D : 0.5764705882352941D;
                double d2 = animal.isBaby() ? random.nextGaussian() * 0.2D : 0.19215686274509805D;

                for (int i = 0; i < 10; i++)
                    animal.level.addParticle(particle, animal.getRandomX(1.0D), animal.getRandomY() + 0.5D, animal.getRandomZ(1.0D), d0, d1, d2);
            }
        }
    }
}
