package space.uncountablyinfinite.foreveryoung.common.events;

import net.minecraft.entity.passive.AnimalEntity;
import net.minecraftforge.event.entity.EntityTravelToDimensionEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import space.uncountablyinfinite.foreveryoung.ForeverYoung;

@EventBusSubscriber(modid = ForeverYoung.MOD_ID, bus = Bus.FORGE)
public class SterileAnimals {

    @SubscribeEvent
    public static void cullSterileAnimals(LivingDeathEvent event) {
        if (event.getEntityLiving() instanceof AnimalEntity) {
            ForeverYoung.STERILE_ANIMALS.kill((AnimalEntity) event.getEntityLiving());
        }
    }

    @SubscribeEvent
    public static void swapWorlds(EntityTravelToDimensionEvent event) {
        if (event.getEntity() instanceof AnimalEntity && ForeverYoung.STERILE_ANIMALS.isSterile((AnimalEntity) event.getEntity())) {
            ForeverYoung.STERILE_ANIMALS.travel((AnimalEntity) event.getEntity(), event.getDimension());
        }
    }
}
