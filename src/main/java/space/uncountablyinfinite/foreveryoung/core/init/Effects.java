package space.uncountablyinfinite.foreveryoung.core.init;

import net.minecraft.potion.EffectInstance;

public class Effects {
    public static final EffectInstance TEN_MINUTE_NIGHT_VISION = new EffectInstance(net.minecraft.potion.Effects.NIGHT_VISION, 12000, 1);
}
