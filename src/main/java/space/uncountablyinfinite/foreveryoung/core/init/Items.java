package space.uncountablyinfinite.foreveryoung.core.init;

import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import space.uncountablyinfinite.foreveryoung.ForeverYoung;
import space.uncountablyinfinite.foreveryoung.common.items.SunflowerStew;

public class Items {
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS,
            ForeverYoung.MOD_ID);
    public static final RegistryObject<Item> SUNFLOWER_STEW = ITEMS.register("sunflower_stew", () -> new SunflowerStew());
}
