package space.uncountablyinfinite.foreveryoung.core.init;

import net.minecraft.item.Food;

public class Foods {
    public static final Food SUNFLOWER_STEW = (new Food.Builder()).nutrition(6).saturationMod(0.6F).alwaysEat().effect(() -> Effects.TEN_MINUTE_NIGHT_VISION, 1.0F).build();
}
