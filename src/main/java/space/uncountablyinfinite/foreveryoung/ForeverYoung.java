package space.uncountablyinfinite.foreveryoung;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import space.uncountablyinfinite.foreveryoung.common.data.SterileAnimals;
import space.uncountablyinfinite.foreveryoung.core.init.Items;

@Mod(ForeverYoung.MOD_ID)
public class ForeverYoung  {
    public static final SterileAnimals STERILE_ANIMALS = new SterileAnimals();
    public static final Logger LOGGER = LogManager.getLogger();
    public static final String MOD_ID = "foreveryoung";

    public ForeverYoung() {
        IEventBus event_bus = FMLJavaModLoadingContext.get().getModEventBus();
        event_bus.addListener(this::setup);

        Items.ITEMS.register(event_bus);

        MinecraftForge.EVENT_BUS.register(this);
    }

    private void setup(final FMLCommonSetupEvent event) {
    }
}
